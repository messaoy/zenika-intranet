import React from 'react';
import './SlackIntegration.scss';

const SlackIntegration = () => (
  <section className="container">
    <h1 className="titre">Integration API Slack</h1>
    <section className="slack" />
  </section>
);

export default SlackIntegration;
