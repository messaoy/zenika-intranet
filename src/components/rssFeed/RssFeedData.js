import React from 'react';
import '../layout/Layout.scss';

const RssFeedData = () => (
  <section className="section box mbmd">
    <h1 className="title is-4 is-spaced">Feed 1 - Skynet news</h1>
    <a type="application/rss+xml" title="RSS Feed" href="https://www.sourcedomain.com/feed/" rel="noopener noreferrer" target="_blank">https://www.sourcedomain.com/feed/</a>
  </section>
);

export default RssFeedData;
