import React, { Component } from 'react';
import '../app/index.scss';
import '../app/App.scss';
import Caroussel from '../caroussel/Caroussel';

class Home extends Component {
  render() {
    return (
      <Caroussel />
    );
  }
}

export default Home;
