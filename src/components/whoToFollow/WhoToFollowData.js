import React from 'react';
import '../layout/Layout.scss';

const RessourceData = (props) => (
  <section className="section box mbmd">
    <h1 className="title is-4 is-spaced">Jean TASSE</h1>
    <section>
      <a href="https://github.com" rel="noopener noreferrer" target="_blank">GitHub</a>
    </section>
    <section>
      <a href="https://facebook.com" rel="noopener noreferrer" target="_blank">Facebook</a>
    </section>
    <section>
      <a href="https://twitter.com" rel="noopener noreferrer" target="_blank">Twitter</a>
    </section>
  </section>
);

export default RessourceData;
